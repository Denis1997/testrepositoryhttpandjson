#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "restclient-cpp/connection.h"
#include "restclient-cpp/restclient.h"

#include <cstdio>
using namespace rapidjson;

int main() {
    // 1. Parse a JSON string into DOM.
    //const char* json = "{\"project\":\"rapidjson\",\"stars\":10}";
	RestClient::Response r = RestClient::get("http://denis:12345678@localhost:5984/_all_dbs");
	printf("%d\n", r.code);
	printf("%s\n", r.body.c_str());
	r = RestClient::put("http://denis:12345678@localhost:5984/authors", "", "");
	printf("\n-----------------\n%d\n", r.code);
	printf("%s\n", r.body.c_str());
	r = RestClient::get("http://denis:12345678@localhost:5984/_uuids");
    Document d;
	d.Parse("{}");
	const char *body = r.body.c_str();
	//d.Parse(r.body.c_str());
	//printf("%s", d["uuids"]);
	//RestClient::post("http://denis:12345678@localhost:5984/authors -d "")
     //d.Parse(json);
     // 2. Modify it by DOM.
	 Value author;
	 char name[20];
	 int len = sprintf(name, "%s %s", "Denis", "Shilo");
	 author.SetString(name, static_cast<SizeType>(len), d.GetAllocator());
	 d.AddMember("author", author, d.GetAllocator());
	 Value pi;
	 pi.SetDouble(3.14159);
	 d.AddMember("pi", pi, d.GetAllocator());
	 Value happy;
	 happy.SetBool(true);
	 d.AddMember("happy", happy, d.GetAllocator());
	 //d["happy"].SetBool(true);
     // 3. Stringify the DOM
     StringBuffer buffer;
     Writer<StringBuffer> writer(buffer);
     d.Accept(writer);
	 
     printf("%s\n", buffer.GetString());
    return 0;
}